import React, { Component } from 'react'
import browserData from './browserData'
import getIEVersion from './getIEVersion'

class IeChecker extends Component {
    render() {
        const ieVersion = typeof window !== 'undefined' ?  getIEVersion(window.navigator.userAgent) : 0

        if (ieVersion !== 0) {
            const items = browserData.map((val, key) => (
                <div className="browser-items" key={key}>
                    <a href={val.link} target="_blank" rel="noopener noreferrer">
                        <img src={val.img} className="browser-items--image" />
                    </a>
                    <div className="browser-items-info">
                        <a href={val.link} target="_blank" rel="noopener noreferrer">
                            <p className="browser-items-info--title">{val.title}</p>
                        </a>
                        <p className="browser-items-info--subtitle">{val.subtitle}</p>
                    </div>
                </div>
            ))

            return (
                <div className="ie-check">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <h1 className="ie-check--title">Vaš preglednik više nije podržan</h1>
                                <p className="ie-check--text">
                                Vidimo da koristite zastarjeli <b>Internet Explorer {ieVersion} </b> kao Vaš preglednik.<br /><br /> Naše stranice rade na svim modernim preglednicima kako bi iskoristili najnovije mogućnosti moderne tehnologije. Molimo Vas da promijenite preglednik na neki moderniji kako bi ste neometano mogli nastaviti koristiti naše stranice.
                                </p>
                                <div className="browser-item-container">
                                    <h2 className="browser-item-container--title">PREUZMITE POSLJEDNJU VERZIJU</h2>
                                    {items}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return null
    }
}

export default IeChecker
