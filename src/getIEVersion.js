const getIEVersion = (userAgentString) => {
    const idx = userAgentString.indexOf('MSIE')

    if (idx !== -1) {
        const ver = parseInt(userAgentString.substring(idx + 5, userAgentString.indexOf('.', idx)), 10)
        if(!isNaN(ver)){
            return ver
        }

        return 0
    }

    return 0
}

export default getIEVersion