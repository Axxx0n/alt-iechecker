import test from 'ava'
require('babel-register')({
    presets: [ 'es2015', 'stage-0' ]
  })
  
import getIEVersion from '../src/getIEVersion'

test('test IE version', (t) => {
    const testCases = [
        // chrome
        {
            input: 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
            result: 0
        },
        // ie 10
        {
            input: 'Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0',
            result: 10
        },
        // ie 9
        {
            input: 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/4.0; GTB7.4; InfoPath.3; SV1; .NET CLR 3.1.76908; WOW64; en-US)',
            result: 9
        },
        // ie 8
        {
            input: 'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; GTB7.4; InfoPath.2; SV1; .NET CLR 3.3.69573; WOW64; en-US)',
            result: 8
        },
        // firefox
        {
            input: 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
            result: 0
        },
        // random
        {
            input: 'Mozilla/5.0 (compatible; MSIE',
            result: 0
        },
        // random
        {
            input: 'Mozilla/5.0 (compatible; MSIE .',
            result: 0
        },
    ]

    for(let i = 0; i < testCases.length; i += 1) {
        console.log(i)
        t.is(getIEVersion(testCases[i].input), testCases[i].result)
    }
})
