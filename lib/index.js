'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _browserData = require('./browserData');

var _browserData2 = _interopRequireDefault(_browserData);

var _getIEVersion = require('./getIEVersion');

var _getIEVersion2 = _interopRequireDefault(_getIEVersion);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var IeChecker = function (_Component) {
    _inherits(IeChecker, _Component);

    function IeChecker() {
        _classCallCheck(this, IeChecker);

        return _possibleConstructorReturn(this, (IeChecker.__proto__ || Object.getPrototypeOf(IeChecker)).apply(this, arguments));
    }

    _createClass(IeChecker, [{
        key: 'render',
        value: function render() {
            var ieVersion = typeof window !== 'undefined' ? (0, _getIEVersion2.default)(window.navigator.userAgent) : 0;

            if (ieVersion !== 0) {
                var items = _browserData2.default.map(function (val, key) {
                    return _react2.default.createElement(
                        'div',
                        { className: 'browser-items', key: key },
                        _react2.default.createElement(
                            'a',
                            { href: val.link, target: '_blank', rel: 'noopener noreferrer' },
                            _react2.default.createElement('img', { src: val.img, className: 'browser-items--image' })
                        ),
                        _react2.default.createElement(
                            'div',
                            { className: 'browser-items-info' },
                            _react2.default.createElement(
                                'a',
                                { href: val.link, target: '_blank', rel: 'noopener noreferrer' },
                                _react2.default.createElement(
                                    'p',
                                    { className: 'browser-items-info--title' },
                                    val.title
                                )
                            ),
                            _react2.default.createElement(
                                'p',
                                { className: 'browser-items-info--subtitle' },
                                val.subtitle
                            )
                        )
                    );
                });

                return _react2.default.createElement(
                    'div',
                    { className: 'ie-check' },
                    _react2.default.createElement(
                        'div',
                        { className: 'container' },
                        _react2.default.createElement(
                            'div',
                            { className: 'row' },
                            _react2.default.createElement(
                                'div',
                                { className: 'col-md-12' },
                                _react2.default.createElement(
                                    'h1',
                                    { className: 'ie-check--title' },
                                    'Va\u0161 preglednik vi\u0161e nije podr\u017Ean'
                                ),
                                _react2.default.createElement(
                                    'p',
                                    { className: 'ie-check--text' },
                                    'Vidimo da koristite zastarjeli ',
                                    _react2.default.createElement(
                                        'b',
                                        null,
                                        'Internet Explorer ',
                                        ieVersion,
                                        ' '
                                    ),
                                    ' kao Va\u0161 preglednik.',
                                    _react2.default.createElement('br', null),
                                    _react2.default.createElement('br', null),
                                    ' Na\u0161e stranice rade na svim modernim preglednicima kako bi iskoristili najnovije mogu\u0107nosti moderne tehnologije. Molimo Vas da promijenite preglednik na neki moderniji kako bi ste neometano mogli nastaviti koristiti na\u0161e stranice.'
                                ),
                                _react2.default.createElement(
                                    'div',
                                    { className: 'browser-item-container' },
                                    _react2.default.createElement(
                                        'h2',
                                        { className: 'browser-item-container--title' },
                                        'PREUZMITE POSLJEDNJU VERZIJU'
                                    ),
                                    items
                                )
                            )
                        )
                    )
                );
            }
            return null;
        }
    }]);

    return IeChecker;
}(_react.Component);

exports.default = IeChecker;