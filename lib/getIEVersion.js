'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var getIEVersion = function getIEVersion(userAgentString) {
    var idx = userAgentString.indexOf('MSIE');

    if (idx !== -1) {
        var ver = parseInt(userAgentString.substring(idx + 5, userAgentString.indexOf('.', idx)), 10);
        if (!isNaN(ver)) {
            return ver;
        }

        return 0;
    }

    return 0;
};

exports.default = getIEVersion;